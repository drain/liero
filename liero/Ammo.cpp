#include "Ammo.h"
#include <cmath>

Ammo::Ammo(sf::Color col, float r, AmmoType type) 
{
	this->shape.setFillColor(col);
	this->shape.setRadius(r);
	this->type = type;

	//TODO: ammo spawn gets drawn instantly when spawned, fix!
	this->shape.setPosition(-20, -20);

};

void Ammo::fire(bool playerShot) 
{
	this->playerShot = playerShot;
	// set the velocity of the ammo correctly
	double xdiff = this->aim_x - this->x;
	double ydiff = this->aim_y - this->y;

	double hyp = sqrt(xdiff*xdiff + ydiff*ydiff);
	// the total velocity is 1.0
	this->v_x = GameValues::ammoSpeed * xdiff / hyp;
	this->v_y = GameValues::ammoSpeed * ydiff / hyp;

	this->useGravity = true;
	
	// set this ammo LIVE!!!
	this->bLive = true;
};

void Ammo::update(float deltaTime) 
{
	//if ammo is live, update the logic
	if (this->bLive) {
		this->x += v_x * deltaTime;
		this->y += v_y * deltaTime;
		GameObject::update(deltaTime);
	}

	this->shape.setPosition(x - 5.0f, y - 5.0f);
};

// Overloaded reset()-method for ammo
void Ammo::Reset(double x, double y) 
{
	GameObject::SetPosition(x, y); // Call the reset() from GameObject-class first

	//this->useGravity = false;
	this->bLive = false;
};

//returns color of the map pixel hit, returns white if it goes out of range
sf::Color Ammo::checkCollisionWithLevel(Map map)
{
	// If the position of the ammo collides with level object(s)

	// Check if the ammo coords are off screen
	if (this->getX() < 0 || this->getX() >= map.GetWidth() ||
		this->getY() < 0 || this->getY() >= map.GetHeigth())
		return sf::Color::White;

	if (map.mapData->getPixel(this->getX(), this->getY()) == GameValues::breakAbleColor)
		return GameValues::breakAbleColor;
	if(map.mapData->getPixel(this->getX(), this->getY()) == GameValues::unbreakAbleColor)
		return GameValues::unbreakAbleColor;

	return sf::Color::White;
};

bool Ammo::checkOutOfBounds(Map map) 
{
	// Check if the ammo coords are off screen
	if (this->getX() < 0 || this->getX() >= map.GetWidth() ||
		this->getY() < 0 || this->getY() >= map.GetHeigth()) {
		this->bLive = false;
		return true;
	}
	return false;
};
