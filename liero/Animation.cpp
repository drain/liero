#include "Animation.h"

Animation::Animation(sf::Sprite* target) : target(target) 
{
	progress = totalLength = 0.0;
}

//add new frame to animation
void Animation::addFrame(Frame&& frame) 
{
	totalLength += frame.duration;
	frames.push_back(std::move(frame));
}

//update animation based on elapsed frame time
void Animation::update(double elapsed)
{
	progress += elapsed;
	double p = progress;
	for (size_t i = 0; i < frames.size(); i++)
	{
		// We check if p has some time left at the end of the animation...
		if (playbackMode == PlaybackMode::Loop && p > 0.0 && &(frames[i]) == &(frames.back()))
		{
			i = 0;    // start over from the beginning
			progress = 0;
			p = progress;
			continue; // break off the loop and start where i is
		}

		p -= frames[i].duration;

		// if we have progressed OR if we're on the last frame, apply and stop.
		if (p <= 0.0 || &(frames[i]) == &frames.back())
		{
			target->setTextureRect(frames[i].rect);
			if(changeOrigin)
				target->setOrigin((frames[i].rect.width) / 2, (frames[i].rect.height) / 2);
			break;
		}
	}
}