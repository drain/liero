#pragma once
#include "SFML/Graphics.hpp"

struct Frame 
{
	sf::IntRect rect;
	double duration; // in seconds
};

enum PlaybackMode 
{
	Once,
	Loop
};

/*Modified version of https://github.com/SFML/SFML/wiki/Easy-Animations-With-Spritesheets */
class Animation 
{
private:
	std::vector<Frame> frames;
	double totalLength;
	double progress;
	sf::Sprite* target;

public:
	Animation(sf::Sprite* target);
	Animation(){};
	//virtual ~Animation();
	PlaybackMode playbackMode;
	void addFrame(Frame&& frame);
	void update(double elapsed);
	void setTarget(sf::Sprite* target) { this->target = target; }
	const double getLength() const { return totalLength; }

	//default true
	bool changeOrigin = true;
};