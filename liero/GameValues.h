#pragma once
#include "SFML/Graphics.hpp"

class GameValues
{
public:
	static sf::Color emptyColor;
	static sf::Color unbreakAbleColor;
	static sf::Color breakAbleColor;
	static sf::Color ammoColor;
	static sf::Color heavyAmmoColor;
	static sf::Color playerColor;
	static sf::Color enemyColor;

	static float winCondition;

	static float gravity;

	static float playerSpeed;
	static float playerJump;
	static float fallingSpeed;

	static float playerMaxHealth;
	static float enemyMaxHealth;

	static float explosionRadius;
	static float heavyAmmoRadius;

	static float ammoSpeed;
	static float heavyAmmoSpeed;
	static float enemyShootDelay;
	static float playerShootDelay;
	
	static float playerHeavyShootDelay;
	static float enemyHeavyShootDelay;

	static float blockAmount;
	static float minBlockW;

	static sf::Time PHYSICS_TIME_STEP;
};
