#include "Game.h"
#include "iostream"


Game::Game(int windowW, int windowH)
{
	soundManager = new SoundManager();
	rend = new Renderer(windowW, windowH, this);
	gameMap = new Map(this);
	gameMap->SetSize(windowW, windowH);
	player = new Player(this);
	player->setSM(soundManager);
	inputHandler = new InputHandler(rend->getWindow(), this);
	enemy = new Enemy(this);

	explosion = new Explosion(GameValues::explosionRadius, this, 20);
	heavyExplosion = new Explosion(GameValues::heavyAmmoRadius, this, 40);

	explosionText.loadFromFile("sprites/explosion.png");

	//setup particle systems
	//TODO: Make same type of system for explosions that is in place for ammo
	//for now we only use 2 particle systems
	smallAmmoParticle = new ParticleSystemWithTexture(30);

	sf::VertexArray vArr(sf::Quads, 4);
	vArr[0].position = sf::Vector2f(-10.0, -10.0);
	vArr[1].position = sf::Vector2f(10.0, -10.0);
	vArr[2].position = sf::Vector2f(10.0, 10.0);
	vArr[3].position = sf::Vector2f(-10.0, 10.0);

	vArr[0].texCoords = sf::Vector2f(0.0f, 0.0f);
	vArr[1].texCoords = sf::Vector2f(34.0f, 0.0f);
	vArr[2].texCoords = sf::Vector2f(34.0f, 34.0f);
	vArr[3].texCoords = sf::Vector2f(0.0f, 34.0f);

	smallAmmoParticle->setBaseQuad(vArr);
	smallAmmoParticle->setTexture(explosionText);

	smallAmmoParticle->setLifetime(250);
	smallAmmoParticle->setColor(GameValues::ammoColor);//sf::Color(61, 61, 61));
	smallAmmoParticle->setSpread(360);
	smallAmmoParticle->setSpeed(100);

	heavyAmmoParticle = new ParticleSystemWithTexture(75);

	sf::VertexArray hvArr(sf::Quads, 4);
	hvArr[0].position = sf::Vector2f(-10.0, -10.0);
	hvArr[1].position = sf::Vector2f(10.0, -10.0);
	hvArr[2].position = sf::Vector2f(10.0, 10.0);
	hvArr[3].position = sf::Vector2f(-10.0, 10.0);

	hvArr[0].texCoords = sf::Vector2f(0.0f, 0.0f);
	hvArr[1].texCoords = sf::Vector2f(34.0f, 0.0f);
	hvArr[2].texCoords = sf::Vector2f(34.0f, 34.0f);
	hvArr[3].texCoords = sf::Vector2f(0.0f, 34.0f);

	heavyAmmoParticle->setBaseQuad(hvArr);
	heavyAmmoParticle->setTexture(explosionText);

	heavyAmmoParticle->setLifetime(300);
	heavyAmmoParticle->setColor(GameValues::heavyAmmoColor);
	heavyAmmoParticle->setSpread(360);
	heavyAmmoParticle->setSpeed(225);

	mainMenuText.loadFromFile("sprites/mainmenu.png");
	mainMenuSprite.setTexture(mainMenuText);
}

Game::~Game()
{
	if(rend != nullptr)
		delete rend;
	if(gameMap != nullptr)
		delete gameMap;
	if (player != nullptr)
		delete player;
	if (inputHandler != nullptr)
		delete inputHandler;
	if (enemy != nullptr)
		delete enemy;
	if (explosion != nullptr)
		delete explosion;
	if (heavyExplosion != nullptr)
		delete heavyExplosion;
	if (smallAmmoParticle != nullptr)
		delete smallAmmoParticle;
	if (heavyAmmoParticle != nullptr)
		delete heavyAmmoParticle;
}

//TODO: Move this to renderer
//Draws main menu
void Game::DisplayMainMenu()
{
	rend->getWindow()->clear();
	mainMenuSprite.setPosition(20, 0);
	mainMenuSprite.setScale( this->GetMap()->GetWidth() / mainMenuSprite.getLocalBounds().width,
		this->GetMap()->GetHeigth() / mainMenuSprite.getLocalBounds().height );
	rend->getWindow()->draw(mainMenuSprite);
	rend->getWindow()->display();
}

/*init game here*/
void Game::Start()
{
	gState = GameState::menu;
	GameIsActive = false;

	//TODO: Different music in menu and game?
	soundManager->playMusic();

	rend->getWindow()->setKeyRepeatEnabled(true);

	//https://stackoverflow.com/questions/20957630/my-sfml-c-program-is-working-as-expected-but-how-to-make-movement-slow
	//https://en.sfml-dev.org/forums/index.php?topic=15613.msg111341#msg111341
	//https://en.sfml-dev.org/forums/index.php?topic=15676.0
	//rend->getWindow()->setFramerateLimit(60);

	//MAIN GAME LOOP
	while (rend->getWindow()->isOpen())
	{
		//process player inputs, timestep is passed due shooting logic, that way ammo gets deltatime
		//there is a better design most likely, but im too lazy
		inputHandler->ProcessInput(GameValues::PHYSICS_TIME_STEP.asSeconds());

		if (gState == GameState::menu)
		{
			//handle main menu

			//TODO: move drawing to renderer
			DisplayMainMenu();
			
		}
		else if (gState == GameState::pauseMenu)
		{
			//TODO: move this to renderer
			rend->DrawPauseMenu();
			rend->getWindow()->display();
		}
		else if (gState == GameState::game)
		{
			//handle game

			//if we enter game and game is not active yet
			if (!GameIsActive)
			{
				ReloadGame();
				GameIsActive = true;

				//restart clock when we start the game
				clock.restart();
			}
			
			float deltaTime = clock.restart().asSeconds(); 
			elapsed += deltaTime;

			//draw the game
			rend->DrawWindow();

			//update game logic on fixed time step
			while (elapsed >= GameValues::PHYSICS_TIME_STEP.asSeconds() && GameIsActive)
			{
				Update(GameValues::PHYSICS_TIME_STEP.asSeconds());
				elapsed -= GameValues::PHYSICS_TIME_STEP.asSeconds();
			}
		}

		//lower cpu usage? not sure if true tho
		sf::sleep(sf::microseconds(1));
	}
}

/*update game logic*/
void Game::Update(float deltaTime)
{
	if (!GameIsActive && !GameIsPaused)
		return;

	if(restart)
		restartTimer += deltaTime;
	if (restartTimer >= restartTimerMax)
	{
		restartTimer = 0;

		if (playerWon || enemyWon)
			GoToMenu();
		else
			ReloadGame();
		
	}

	//update screen shake
	UpdateScreenShake(GameValues::PHYSICS_TIME_STEP.asSeconds());

	//update player collisions
	UpdatePlayerCollisions();

	//update enemy collisions (maybe loop later, if we decide to implement multiple enemies
	UpdateEnemyCollisions();

	//Update player physics and logic
	player->Update(gameMap->GetWidth(),gameMap->GetHeigth(), deltaTime);

	//update enemy physics and logic
	enemy->Update(gameMap->GetWidth(), gameMap->GetHeigth(), deltaTime);

	//TODO: optimize this
	CheckAmmoCollisions(deltaTime);

	//TODO: multiple particle systems create/delete
	getParticleSystem()->update(GameValues::PHYSICS_TIME_STEP);
	getHeavyParticleSystem()->update(GameValues::PHYSICS_TIME_STEP);

	// Clean up our ammo vector
	for (auto it = vec_ammo.begin(); it != vec_ammo.end();) {
		if (!(*it)->isLive())
			it = vec_ammo.erase(it);
		else
			it++;
	}
}

Map* Game::GetMap()
{
	return gameMap;
}

Renderer* Game::GetRenderer()
{
	return rend;
}

Player* Game::GetPlayer()
{
	return player;
}

Enemy* Game::GetEnemy()
{
	return this->enemy;
}

//initializes new game, generate map, and reset player and enemy values
//wont reset score
void Game::ReloadGame()
{
	//reset values and generate new map
	ResetViewPos();
	playerWon = false;
	enemyWon = false;
	restart = false;
	gameMap->generateMap();

	GameIsPaused = false;

	//reset player and enemy
	player->Reset();
	
	enemy->Reset();

	player->SetPosition(rand() % (gameMap->GetWidth() - player->width), rand() % (gameMap->GetHeigth() - player->height));
	enemy->SetPosition(rand() % (gameMap->GetWidth() - enemy->width), rand() % (gameMap->GetHeigth() - enemy->height));

	//safe area for player and enemys
	rend->generateCircle(GameValues::emptyColor, GetPlayer()->getX(), GetPlayer()->getY(), 100);
	rend->generateCircle(GameValues::emptyColor, GetEnemy()->getX(), GetEnemy()->getY(), 100);
}

//called when player or enemy dies, restarts round or checks who won
void Game::RestartGame()
{
	restart = true;
	if (playerScore >= GameValues::winCondition)
	{
		playerWon = true;
	}
	else if (enemyScore >= GameValues::winCondition)
	{
		enemyWon = true;
	}
}

//change game state to menu
void Game::GoToMenu()
{
	playerScore = 0;
	enemyScore = 0;
	gState = GameState::menu;
	GameIsActive = false;
	restart = false;
	ResetScore();
}

//change game state to pause menu and pause the game
void Game::Pause()
{
	GameIsPaused = true;
	gState = GameState::pauseMenu;
	//stop game clock somehow? not sure if possible in smfl
}

//change state back to game and restart game clock
void Game::Continue()
{
	GameIsPaused = false;
	gState = Game::GameState::game;

	//restart game clock
	clock.restart();
}

//reset player and enemy scores
void Game::ResetScore()
{
	playerScore = 0;
	enemyScore = 0;
}

//start screen shake based on wanted power
void Game::ScreenShake(float power)
{
	sPower = power;
	shakeScreen = true;
}

//reset view position to middle of the screen
void Game::ResetViewPos()
{
	shakeScreen = false;
	// reset to original position
	sf::View view = sf::View(rend->getWindow()->getView());
	//view.setViewport(sf::FloatRect(0,0,0.5f,0));
	view.setCenter(gameMap->GetWidth() / 2, gameMap->GetHeigth() / 2);
	rend->getWindow()->setView(view);
}

//update screen shake based on timer
void Game::UpdateScreenShake(float deltaTime)
{
	if (!shakeScreen)
		return;

	if (shakeTimer <= maxShakeTimer)
	{
		shakeTimer += deltaTime;

		sf::View view = sf::View(rend->getWindow()->getView());
		sf::Vector2f shake = sf::Vector2f((rand() % 3 - 1) * sPower, (rand() % 3 - 1) * sPower);
		//std::cout << shake.x << ", " << shake.y << std::endl;

		view.move(shake);

		rend->getWindow()->setView(view);
	}
	else
	{
		shakeTimer = 0;
		shakeScreen = false;
		ResetViewPos();
	}
}

/*AMMO COLLISIONS ---------------------------------------*/

//spawns explosion on ammo position
void Game::AmmoCollided(Ammo* ammo)
{
	if (ammo->type == AmmoType::normal)
	{
		this->explosion->SetPosition(ammo->getX() - this->explosion->radius, ammo->getY() - this->explosion->radius);
		this->explosion->explode(this->gameMap->mapData, this->gameMap->GetWidth(), this->gameMap->GetHeigth(), ammo->playerShot);
		gameMap->setDirty(true); // Set level dirty, so it gets updated in renderer
		soundManager->playSound("explosion");

		getParticleSystem()->setEmitter(sf::Vector2f(ammo->getX(), ammo->getY()));
		getParticleSystem()->resetParticles();

		ScreenShake(.5f);
	}
	else if (ammo->type == AmmoType::heavy)
	{
		this->heavyExplosion->SetPosition(ammo->getX() - this->heavyExplosion->radius, ammo->getY() - this->heavyExplosion->radius);
		this->heavyExplosion->explode(this->gameMap->mapData, this->gameMap->GetWidth(), this->gameMap->GetHeigth(), ammo->playerShot);
		gameMap->setDirty(true); // Set level dirty, so it gets updated in renderer

		soundManager->playSound("hExplosion");

		getHeavyParticleSystem()->setEmitter(sf::Vector2f(ammo->getX(), ammo->getY()));
		getHeavyParticleSystem()->resetParticles();

		ScreenShake(2);
	}

	// Kill/reset the ammo
	ammo->Reset(this->player->getX(), this->player->getY());
}

//check collisions for alive ammos
void Game::CheckAmmoCollisions(float deltaTime)
{
	for (auto it = vec_ammo.begin(); it != vec_ammo.end(); it++) {
		// Ammo collision detection
		sf::Color c = (*it)->checkCollisionWithLevel(*this->gameMap);
		if (c == GameValues::breakAbleColor || c == GameValues::unbreakAbleColor)
		{
			// Move the explosion to correct coords
			AmmoCollided((*it));
		}

		/*Check if player ammo hit player*/
		if (!(*it)->playerShot && (*it)->isLive())
		{
			if ((*it)->getX() >= player->getX() && (*it)->getX() <= (player->getX() + player->width))
			{
				if ((*it)->getY() >= player->getY() && (*it)->getY() <= (player->getY() + player->height))
				{
					AmmoCollided((*it));
				}
			}
		}
		/*Check if enemy ammo hit enemy*/
		else if ((*it)->playerShot && (*it)->isLive())
		{
			if ((*it)->getX() >= enemy->getX() && (*it)->getX() <= (enemy->getX() + enemy->width))
			{
				if ((*it)->getY() >= enemy->getY() && (*it)->getY() <= (enemy->getY() + enemy->height))
				{
					AmmoCollided((*it));
				}
			}
		}

		//Check out of bounds
		if ((*it)->getX() < 0 || (*it)->getX() >= gameMap->GetWidth() ||
			(*it)->getY() < 0 || (*it)->getY() >= gameMap->GetHeigth())
		{
			AmmoCollided((*it));
		}

		//update ammo
		(*it)->update(deltaTime);
	}
}

//TODO: Put player collisions into player and enemy collisions into enemy

#pragma region PlayerCollisions

/*PLAYER COLLISIONS ---------------------------------------------*/
void Game::UpdatePlayerCollisions()
{
	*player->pGetIsGrounded() = false;

	if ((player->getY() + player->height) >= gameMap->GetHeigth())
	{
		*player->pGetIsGrounded() = true;
		*player->pGetCanJump() = true;
		*player->pGetJumpCount() = player->getJumpCountMax();
	}

	if (PlayerCheckCollisionBelow())
	{
		sf::Vector2f speed = player->getSpeed();
		//std::cout << speed.x << ", " << speed.y << std::endl;

		if (speed.y > 0.2f)
		{
			soundManager->playSound("hit");
			speed.y = -speed.y * 0.3f;
			player->SetSpeed(speed);
		}
		else
		{
			speed.y = 0;
			player->SetSpeed(speed);
		}

		*player->pGetIsGrounded() = true;
		*player->pGetCanJump() = true;
		*player->pGetJumpCount() = player->getJumpCountMax();
	}

	if (PlayerCheckCollisionAbove())
	{
		sf::Vector2f speed = player->getSpeed();
		//std::cout << speed.x << ", " << speed.y << std::endl;

		if (speed.y > 0.01f)
			soundManager->playSound("hit");
		speed.y = -speed.y * 0.3f;
		player->SetSpeed(speed);

	}

	if (PlayerCheckCollisionRight())
	{
		player->moveRight = false;
		sf::Vector2f speed = player->getSpeed();
		//std::cout << speed.x << std::endl;

		if (speed.x >= GameValues::playerSpeed)
			soundManager->playSound("hit");
		speed.x = 0;
		player->SetSpeed(speed);

	}

	if (PlayerCheckCollisionLeft())
	{
		player->moveLeft = false;
		sf::Vector2f speed = player->getSpeed();
		//std::cout << speed.x << std::endl;

		if (abs(speed.x) >= GameValues::playerSpeed)
			soundManager->playSound("hit");
		speed.x = 0;
		player->SetSpeed(speed);

	}
}

bool Game::PlayerCheckCollisionBelow()
{
	if (player->getSpeed().y <= 0)
		return false;

	int x = this->GetPlayer()->getX()+2;
	int y = this->GetPlayer()->getY() + this->GetPlayer()->height-1;

	for (int i = x; i < x + this->GetPlayer()->width-2; i++)
		if (y < GetMap()->GetHeigth() && this->GetMap()->mapData->getPixel(i, y) == GameValues::unbreakAbleColor ||
			y < GetMap()->GetHeigth() && this->GetMap()->mapData->getPixel(i, y) == GameValues::breakAbleColor)
			return true;
	return false;
}

bool Game::PlayerCheckCollisionAbove()
{
	if (player->getSpeed().y >= 0)
		return false;

	int x = this->GetPlayer()->getX()+2;
	int y = this->GetPlayer()->getY();

	for (int i = x; i < x + this->GetPlayer()->width-2; i++)
		if (y > 0 && this->GetMap()->mapData->getPixel(i, y) == GameValues::unbreakAbleColor ||
			y > 0 && this->GetMap()->mapData->getPixel(i, y) == GameValues::breakAbleColor)
			return true;
	return false;
}

bool Game::PlayerCheckCollisionRight()
{
	if (player->getSpeed().x <= 0)
		return false;

	int x = this->GetPlayer()->getX() + this->GetPlayer()->width-1;
	int y = this->GetPlayer()->getY()+2;

	for (int i = y; i < y + this->GetPlayer()->height-2; i++)
		if (x < GetMap()->GetWidth() && this->GetMap()->mapData->getPixel(x, i) == GameValues::unbreakAbleColor ||
			x < GetMap()->GetWidth() && this->GetMap()->mapData->getPixel(x, i) == GameValues::breakAbleColor)
			return true;
	return false;
}

bool Game::PlayerCheckCollisionLeft()
{
	if (player->getSpeed().x >= 0)
		return false;

	int x = this->GetPlayer()->getX();
	int y = this->GetPlayer()->getY()+2;

	for (int i = y; i < y + this->GetPlayer()->height-2; i++)
		if (x > 0 && this->GetMap()->mapData->getPixel(x, i) == GameValues::unbreakAbleColor ||
			x > 0 && this->GetMap()->mapData->getPixel(x, i) == GameValues::breakAbleColor)
			return true;
	return false;
}

#pragma endregion PlayerCollisions

#pragma region EnemyCollisions

/*ENEMY COLLISIONS ---------------------------------------------*/
void Game::UpdateEnemyCollisions()
{
	*enemy->pGetIsGrounded() = false;

	if ((enemy->getY() + enemy->height) >= gameMap->GetHeigth())
	{
		*enemy->pGetIsGrounded() = true;
	}

	if (EnemyCheckCollisionAbove(enemy))
	{
		sf::Vector2f speed = enemy->getSpeed();
		speed.y = -speed.y*.3f;
		enemy->SetSpeed(speed);
	}
	if (EnemyCheckCollisionBelow(enemy))
	{
		//TODO: make this better
		sf::Vector2f speed = enemy->getSpeed();
		if (speed.y > 0.2f)
		{
			speed.y = -speed.y * 0.3f;
			enemy->SetSpeed(speed);
		}
		else
		{
			speed.y = 0;
			enemy->SetSpeed(speed);
		}

		*enemy->pGetIsGrounded() = true;
	}
	if (EnemyCheckCollisionRight(enemy))
	{
		sf::Vector2f speed = enemy->getSpeed();
		speed.x = -speed.x*.1f;
		enemy->SetSpeed(speed);
	}
	if (EnemyCheckCollisionLeft(enemy))
	{
		sf::Vector2f speed = enemy->getSpeed();
		speed.x = -speed.x*.1f;
		enemy->SetSpeed(speed);
	}
}

bool Game::EnemyCheckCollisionBelow(Enemy* enemy)
{
	if (enemy->getSpeed().y <= 0)
		return false;

	int x = enemy->getX() + 2;
	int y = enemy->getY() + enemy->height - 1;

	for (int i = x; i < x + enemy->width - 2; i++)
		if (y < GetMap()->GetHeigth() && this->GetMap()->mapData->getPixel(i, y) == GameValues::unbreakAbleColor ||
			y < GetMap()->GetHeigth() && this->GetMap()->mapData->getPixel(i, y) == GameValues::breakAbleColor)
			return true;
	return false;
}

bool Game::EnemyCheckCollisionAbove(Enemy* enemy)
{
	if (enemy->getSpeed().y >= 0)
		return false;

	int x = enemy->getX() + 2;
	int y = enemy->getY();

	for (int i = x; i < x + enemy->width - 2; i++)
		if (y > 0 && this->GetMap()->mapData->getPixel(i, y) == GameValues::unbreakAbleColor ||
			y > 0 && this->GetMap()->mapData->getPixel(i, y) == GameValues::breakAbleColor)
			return true;
	return false;
}

bool Game::EnemyCheckCollisionRight(Enemy* enemy)
{
	if (enemy->getSpeed().x <= 0)
		return false;

	int x = enemy->getX() + enemy->width - 1;
	int y = enemy->getY() + 2;

	for (int i = y; i < y + enemy->height - 2; i++)
		if (x < GetMap()->GetWidth() && this->GetMap()->mapData->getPixel(x, i) == GameValues::unbreakAbleColor ||
			x < GetMap()->GetWidth() && this->GetMap()->mapData->getPixel(x, i) == GameValues::breakAbleColor)
			return true;
	return false;
}

bool Game::EnemyCheckCollisionLeft(Enemy* enemy)
{
	if (enemy->getSpeed().x >= 0)
		return false;

	int x = enemy->getX();
	int y = enemy->getY() + 2;

	for (int i = y; i < y + enemy->height - 2; i++)
		if (x > 0 && this->GetMap()->mapData->getPixel(x, i) == GameValues::unbreakAbleColor ||
			x > 0 && this->GetMap()->mapData->getPixel(x, i) == GameValues::breakAbleColor)
			return true;
	return false;
}

#pragma endregion EnemyCollisions
