#pragma once
#include "GameObject.h"
#include "Player.h"
#include "Ammo.h"
#include "iostream"
#include "SoundManager.h"

class Game;

class Enemy : public GameObject
{
private:
	Game* m_game;

	float shootTimer;
	float shootTimerMax;
	float heavyShootTimer;
	float heavyShootTimerMax;

	float health;
	float maxHealth;

	float jumpTimer;
	float jumpTimerMax = 7;

	sf::Texture enemyText;
	sf::Sprite enemySprite;

	Animation idleAnim;
	Animation jumpAnim;
	Animation runAnim;
	Animation deadAnim;

	bool isFacingLeft = false;
	bool isDead = false;
	bool isGrounded = false;

	float dmgDrawTimer = 0;
	float dmgDrawTimerMax = 1;
	bool showDmg = false;
	sf::Vector2f dmgPos;

	float lastDmgTaken;
	void UpdateShowDmg();

public:
	Enemy(Game* game);

	const int width = 20;
	const int height = 40;

	void SetSpeed(sf::Vector2f speed);

	void Reset();
	void Update(int mapW, int mapH, float deltaTime);
	void Shoot();
	void ShootHeavy();

	void TakeDmg(float dmg);

	float getHealth() { return health; }
	float getMaxHealth() { return maxHealth; }
	void setHealth(float health) { this->health = health; };

	void UpdateAnimation(float deltaTime);

	void jump(float deltaTime);

	/*TODO: Make getters and setters instead of pointers*/
	float getLastDmgTaken() { return this->lastDmgTaken; };

	void ShowDmg(float dmg);

	sf::Vector2f getDmgPos() { return this->dmgPos; };

	sf::Sprite* getEnemySprite() { return &enemySprite; };

	bool getFacingLeft() { return isFacingLeft; };
	bool getShowDmg() { return showDmg; };

	bool* pGetIsGrounded() { return &isGrounded; };

	//bool* pGetIsDead() { return &isDead; };
};