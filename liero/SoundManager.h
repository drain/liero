#pragma once
#include "iostream"
#include "string"
#include "SFML/Audio.hpp"

class SoundAsset
{
private:
	std::string path;
	sf::SoundBuffer buffer;
	//sf::Sound sound;

public:
	SoundAsset(std::string name, std::string path) 
	{ 
		this->name = name;
		this->path = path;

		buffer.loadFromFile(path);
		sound.setBuffer(buffer);
	};

	std::string name;
	sf::Sound sound;

	void play()
	{
		sound.setBuffer(buffer);
		sound.play();
	};
	void stop()
	{
		sound.setBuffer(buffer);
		sound.stop();
	};
};

class SoundManager
{
private:
	std::vector<SoundAsset> sounds;
	sf::Music music;
public: 
	SoundManager();
	void preloadSounds();
	void playSound(std::string name);
	void playMusic();
};

