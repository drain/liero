#include "Renderer.h"
#include "Game.h"
#include "iostream"

Renderer::Renderer(int w, int h, Game* game)
{
	screenWidth = w;
	screenHeight = h;
	this->m_game = game;
	this->window = new sf::RenderWindow(sf::VideoMode(screenWidth, screenHeight), "LIERO");

	if (!arial.loadFromFile("arial.ttf"))
		std::cout << "Cannot load font for some reason.";

}

Renderer::~Renderer()
{
	if (this->window == nullptr)
		delete this->window;
}

sf::RenderWindow* Renderer::getWindow()
{
	return window;
}

/*Main rendering loop*/
void Renderer::DrawWindow()
{
	window->clear();

	/*draw all objects here*/

	// Get the current leveldata from level-object
	if (m_game->GetMap()->isDirty()) {
		m_game->GetMap()->RedrawMap();
		this->m_game->GetMap()->setDirty(false);  // Set dirty bit to false
	}

	//draw map
	window->draw(m_game->GetMap()->mapSprite);

	//draw player
	DrawPlayer(sf::Color::Blue, m_game->GetPlayer()->getX(), m_game->GetPlayer()->getY());

	//draw enemy
	DrawEnemy(sf::Color::Red, m_game->GetEnemy()->getX(), m_game->GetEnemy()->getY());

	//draw dmg text ontop of player and enemy
	if (m_game->GetPlayer()->getShowDmg())
		DrawDmgTaken(m_game->GetPlayer()->getLastDmgTaken(), m_game->GetPlayer()->getDmgPos().x, m_game->GetPlayer()->getDmgPos().y);

	if (m_game->GetEnemy()->getShowDmg())
		DrawDmgTaken(m_game->GetEnemy()->getLastDmgTaken(), m_game->GetEnemy()->getDmgPos().x, m_game->GetEnemy()->getDmgPos().y);

	// Draw each ammo in the vector
	for (auto it = m_game->vec_ammo.begin(); it != m_game->vec_ammo.end(); it++) {
		window->draw((*it)->getShape());
	}

	//draw particles
	window->draw(*m_game->getParticleSystem());
	window->draw(*m_game->getHeavyParticleSystem());

	// Calculate and draw the aim
	double xdiff = m_game->GetPlayer()->aim_x - (m_game->GetPlayer()->getX() + m_game->GetPlayer()->width / 2);
	double ydiff = m_game->GetPlayer()->aim_y - (m_game->GetPlayer()->getY() + m_game->GetPlayer()->height / 2);

	double hyp = sqrt(xdiff*xdiff + ydiff * ydiff);

	// Make the aim coords 50 pixels away from the player
	double x = (m_game->GetPlayer()->getX() + m_game->GetPlayer()->width / 2) + 50.0 * xdiff / hyp;
	double y = (m_game->GetPlayer()->getY() + m_game->GetPlayer()->height / 2) + 50.0 * ydiff / hyp;

	if (x < m_game->GetPlayer()->getX())
		*m_game->GetPlayer()->pGetFacingLeft() = true;
	else
		*m_game->GetPlayer()->pGetFacingLeft() = false;

	/*sf::CircleShape aim;
	aim.setRadius(4);
	aim.setFillColor(sf::Color(74, 194, 99));
	aim.setPosition(x-4, y-4);*/

	m_game->GetPlayer()->pGetCrosshairSprite()->setPosition(x - 11, y - 11);
	window->draw(*m_game->GetPlayer()->pGetCrosshairSprite());

	//TODO: Optimize all UI drawing by only re-rendering it again if the value changed, like in map

	DrawScore("Player: " + std::to_string(m_game->getPlayerScore()) + " Enemy: " + std::to_string(m_game->getEnemyScore()), sf::Color::Black);

	//draw end round text based on who won
	if (m_game->restartPending())
	{
		if (m_game->getPlayerWon())
			DrawGameEndText("PLAYER WON!", sf::Color::Green);
		else if (m_game->getEnemyWon())
			DrawGameEndText("ENEMY WON!", sf::Color::Red);
		else
			DrawGameEndText("ROUND OVER!", sf::Color::Black);
	}

	DrawHealthBar(sf::Color::Green, 10, 10);
	DrawHealthBarEnemy(sf::Color::Green, m_game->GetEnemy()->getX(), m_game->GetEnemy()->getY());

	DisplayFPS(m_game->getElapsedTimeAfterLastFrame());

	window->display();
}


//draw player sprite at x, y postion, also handles flipping of playerSprite
void Renderer::DrawPlayer(sf::Color col, int x, int y)
{
	//sf::RectangleShape player;
	//player.setSize(sf::Vector2f(m_game->getPlayer()->width, m_game->getPlayer()->height));
	//m_game->getMap()->GetWidth() / 15);
	//player.setFillColor(col);
	//std::cout << *m_game->getPlayer()->getFacingLeft() << std::endl;

	if (*m_game->GetPlayer()->pGetFacingLeft())
		m_game->GetPlayer()->pGetPlayerSprite()->setScale(sf::Vector2f(-1, 1));
	else
		m_game->GetPlayer()->pGetPlayerSprite()->setScale(sf::Vector2f(1, 1));

	//magic numbers here are offsets for the sprite as its not placed directly at the right spot and im too lazy to change
	m_game->GetPlayer()->pGetPlayerSprite()->setPosition(x+12, y+20);

	//m_game->GetPlayer()->UpdateAnimation(m_game->getElapsedTime());

	window->draw(*m_game->GetPlayer()->pGetPlayerSprite());
}

//draw enemy and handle flipping
void Renderer::DrawEnemy(sf::Color col, int x, int y)
{
	if (m_game->GetEnemy()->getFacingLeft())
		m_game->GetEnemy()->getEnemySprite()->setScale(sf::Vector2f(-1, 1));
	else
		m_game->GetEnemy()->getEnemySprite()->setScale(sf::Vector2f(1, 1));

	//magic numbers here are offsets for the sprite as its not placed directly at the right spot and im too lazy to change
	m_game->GetEnemy()->getEnemySprite()->setPosition(x + 5, y + 20);

	//m_game->GetEnemy()->UpdateAnimation(m_game->getElapsedTime());

	window->draw(*m_game->GetEnemy()->getEnemySprite());
}

//draw score screen at the top of the game window
void Renderer::DrawScore(std::string text, sf::Color col)
{
	sf::Text t;
	t.setPosition(screenWidth / 2 - 100, 5);
	t.setFont(arial);
	t.setCharacterSize(24);
	t.setFillColor(col);
	t.setStyle(sf::Text::Bold);

	t.setString(text);

	sf::RectangleShape rs;
	rs.setFillColor(sf::Color::White);
	rs.setPosition(t.getPosition());
	rs.setSize(sf::Vector2f(t.getLocalBounds().width + 5, t.getLocalBounds().height + 5));

	window->draw(rs);
	window->draw(t);
}

//displays FPS at the corner of the screen
void Renderer::DisplayFPS(float deltaTime)
{
	sf::Text t;
	t.setPosition(screenWidth - 70, screenHeight - 20);
	t.setFont(arial);
	t.setCharacterSize(18);
	t.setFillColor(sf::Color(0, 175, 15));
	t.setStyle(sf::Text::Bold);

	//this might not be right? im not 100% sure
	fpsUpdateTimer += deltaTime;
	frameCounter++;
		
	//std::cout << frameCounter << std::endl;

	if (fpsUpdateTimer >= 1)
	{
		fpsPerFrame = frameCounter;
		frameCounter = 0;
		fpsUpdateTimer = 0;
	}

	std::stringstream stream;
	stream << std::fixed << std::setprecision(0) << fpsPerFrame;
	std::string s = stream.str();
	t.setString("fps " + s);

	window->draw(t);
}

//draw round/game end text at the middle of the screen
void Renderer::DrawGameEndText(std::string text, sf::Color col)
{
	sf::Text t;
	t.setPosition(screenWidth / 2 - 165, screenHeight / 2 - 20);
	t.setFont(arial);
	t.setCharacterSize(50);
	t.setFillColor(col);
	t.setStyle(sf::Text::Bold);

	t.setString(text);

	sf::RectangleShape rs;
	rs.setFillColor(sf::Color::White);
	rs.setPosition(t.getPosition());
	rs.setSize(sf::Vector2f(t.getLocalBounds().width + 10, t.getLocalBounds().height + 22.5f));

	window->draw(rs);
	window->draw(t);
}

//draw player healthbar at top left of the screen
void Renderer::DrawHealthBar(sf::Color col, int x, int y)
{
	float ch = m_game->GetPlayer()->getHealth();
	float mh = m_game->GetPlayer()->getMaxHealth();

	sf::RectangleShape healthBarR;
	healthBarR.setSize(sf::Vector2f(200, 30));
	healthBarR.setPosition(x, y);
	healthBarR.setFillColor(sf::Color::Red);

	sf::RectangleShape healthBar;
	healthBar.setSize(sf::Vector2f((ch / mh) * 200, 30));
	healthBar.setPosition(x, y);
	healthBar.setFillColor(col);

	window->draw(healthBarR);
	window->draw(healthBar);

	DrawAmmoCds(sf::Color::Red, healthBar.getPosition().x, healthBar.getPosition().y + 35);
}

void Renderer::DrawAmmoCds(sf::Color col, int x, int y)
{
	sf::Text t;
	t.setPosition(x, y);
	t.setFont(arial);
	t.setCharacterSize(15);
	t.setFillColor(col);
	t.setStyle(sf::Text::Bold);

	//TODO: make a sprite or somehow make this better?
	std::string r = (m_game->GetPlayer()->getHeavyCanShoot()) ? "ready" : "-";
	t.setString("Heavy: " + r);

	window->draw(t);
}

//draw enemy healthbar at top of enemy sprite
void Renderer::DrawHealthBarEnemy(sf::Color col, int x, int y)
{
	float ch = m_game->GetEnemy()->getHealth();
	float mh = m_game->GetEnemy()->getMaxHealth();

	sf::RectangleShape healthBarR;
	healthBarR.setSize(sf::Vector2f(40, 10));
	healthBarR.setPosition(x - 15, y - 15);
	healthBarR.setFillColor(sf::Color::Red);

	sf::RectangleShape healthBar;
	healthBar.setSize(sf::Vector2f((ch / mh) * 40, 10));
	healthBar.setPosition(x - 15, y - 15);
	healthBar.setFillColor(col);

	window->draw(healthBarR);
	window->draw(healthBar);
}

//draw dmg taken number at x, y position
void Renderer::DrawDmgTaken(float dmg, int x, int y)
{	
	sf::Text t;
	//TODO: Add small randomisation?
	t.setPosition(x, y);
	t.setFont(arial);
	t.setCharacterSize(15);
	t.setFillColor(sf::Color::Red);
	t.setStyle(sf::Text::Bold);

	t.setString("-"+std::to_string((int)dmg));

	window->draw(t);
}

//draw pause menu text to screen
void Renderer::DrawPauseMenu()
{
	sf::Text t;
	t.setPosition(screenWidth / 2 - 300, screenHeight / 2 - 100);
	t.setFont(arial);
	t.setCharacterSize(50);
	t.setFillColor(sf::Color::Black);
	t.setStyle(sf::Text::Bold);

	t.setString("GAME PAUSED\nPress Enter to continue\nPress Esc to quit to menu");

	sf::RectangleShape rs;
	rs.setFillColor(sf::Color::White);
	rs.setPosition(t.getPosition());
	rs.setSize(sf::Vector2f(t.getLocalBounds().width + 10, t.getLocalBounds().height + 22.5f));

	window->draw(rs);
	window->draw(t);
}

void Renderer::generateBlock(sf::Color col)
{
	int x = rand() % m_game->GetMap()->GetWidth();
	int y = rand() % m_game->GetMap()->GetHeigth();

	int w = m_game->GetMap()->GetMinBlockW() + rand() % (m_game->GetMap()->GetWidth() / 4);
	int h = m_game->GetMap()->GetMinBlockW() + rand() % (m_game->GetMap()->GetHeigth() / 4);

	for (int j = y; j < y + h; j++)
		for (int i = x; i < x + w; i++)
		{
			if (i < m_game->GetMap()->GetWidth() && j < m_game->GetMap()->GetHeigth())
			{
				sf::Color c = col;
				m_game->GetMap()->mapData->setPixel(i, j, c);
			}
		}
}

void Renderer::generateBlock(sf::Color col, int x, int y, int w, int h)
{
	for (int j = y; j < y + h; j++)
		for (int i = x; i < x + w; i++)
		{
			if (i < m_game->GetMap()->GetWidth() && j < m_game->GetMap()->GetHeigth())
			{
				sf::Color c = col;
				m_game->GetMap()->mapData->setPixel(i, j, c);
			}
		}
}

void Renderer::generateCircle(sf::Color col)
{
	int x = rand() % m_game->GetMap()->GetWidth();
	int y = rand() % m_game->GetMap()->GetHeigth();
	int r = m_game->GetMap()->GetMinBlockW() + (rand() % m_game->GetMap()->GetWidth() / 10);

	for (int j = y - r; j < y + r; j++)
		for (int i = x - r; i < x + r; i++)
		{
			if (i < m_game->GetMap()->GetWidth() && i >= 0 && j < m_game->GetMap()->GetHeigth() && j >= 0)
			{
				if (((i - x)*(i - x) + (j - y)*(j - y)) < (r*r))
				{
					sf::Color c = col;
					m_game->GetMap()->mapData->setPixel(i, j, c);
				}
			}
		}
}

void Renderer::generateCircle(sf::Color col, int x, int y, int r)
{
	for (int j = y - r; j < y + r; j++)
		for (int i = x - r; i < x + r; i++)
		{
			if (i < m_game->GetMap()->GetWidth() && i >= 0 && j < m_game->GetMap()->GetHeigth() && j >= 0)
			{
				if (((i - x)*(i - x) + (j - y)*(j - y)) < (r*r))
				{
					sf::Color c = col;
					m_game->GetMap()->mapData->setPixel(i, j, c);
				}
			}
		}
}

