#include "Enemy.h"
#include "Game.h"


Enemy::Enemy(Game* game)
{
	m_game = game;
	this->maxHealth = GameValues::enemyMaxHealth;
	this->shootTimerMax = GameValues::enemyShootDelay;
	this->heavyShootTimerMax = GameValues::enemyHeavyShootDelay;
	this->health = this->maxHealth;

	// Load the image...
	enemyText.loadFromFile("sprites/soldier2.png");
	enemySprite.setTexture(enemyText);

	//init animation frames
	idleAnim.setTarget(&enemySprite);
	idleAnim.addFrame({ sf::IntRect(0,2,31,48), 0.4 });
	idleAnim.addFrame({ sf::IntRect(0,2,31,48), 0.4 });
	idleAnim.playbackMode = PlaybackMode::Loop;

	jumpAnim.setTarget(&enemySprite);
	jumpAnim.addFrame({ sf::IntRect(205,2,31,48), 0.4 });
	jumpAnim.addFrame({ sf::IntRect(205,2,31,48), 0.4 });

	runAnim.setTarget(&enemySprite);
	runAnim.addFrame({ sf::IntRect(0,58,31,48), 0.1 });
	runAnim.addFrame({ sf::IntRect(54,58,31,48), 0.1 });
	runAnim.addFrame({ sf::IntRect(102,58,31,48), 0.1 });
	runAnim.addFrame({ sf::IntRect(150,58,31,48), 0.1 });
	runAnim.addFrame({ sf::IntRect(258,58,31,48), 0.1 });
	runAnim.addFrame({ sf::IntRect(307,58,31,48), 0.1 });
	runAnim.addFrame({ sf::IntRect(353,58,31,48), 0.1 });
	runAnim.playbackMode = PlaybackMode::Loop;

	deadAnim.setTarget(&enemySprite);
	deadAnim.addFrame({ sf::IntRect(295,2,45,48), 1 });
	deadAnim.addFrame({ sf::IntRect(295,2,45,48), 1 });

}

//trigger dmg text ontop of this
void Enemy::ShowDmg(float dmg)
{
	lastDmgTaken = dmg;
	showDmg = true;
	dmgPos = sf::Vector2f(this->getX() + (rand() % 40 - 20), this->getY() + (rand() % 40 - 20));
}

//update the dmg text
void Enemy::UpdateShowDmg()
{
	if (!showDmg)
		return;

	if (dmgDrawTimer <= dmgDrawTimerMax)
		dmgDrawTimer += GameValues::PHYSICS_TIME_STEP.asSeconds();
	else if (dmgDrawTimer > dmgDrawTimerMax)
	{
		showDmg = false;
		dmgDrawTimer = 0;
	}
}

void Enemy::SetSpeed(sf::Vector2f speed)
{
	this->v_x = speed.x;
	this->v_y = speed.y;
}

void Enemy::Reset()
{
	isDead = false;
	showDmg = false;
	setHealth(getMaxHealth());
	v_x = 0;
	v_y = 0;

	shootTimer = 0;
	heavyShootTimer = 0;
}


void Enemy::Update(int mapW, int mapH, float deltaTime)
{
	//dont update logic if restart is pending
	if (m_game->restartPending())
		return;

	UpdateShowDmg();

	//if grounded, slow our X speed if speed is not applied
	if (this->v_x > 0 && isGrounded)
		this->v_x -= 0.1 * deltaTime;
	else if (this->v_x < 0 && isGrounded)
		this->v_x += 0.1 * deltaTime;

	this->x += this->v_x;
	this->y += this->v_y;

	// Horizontal speed check:
	if (this->v_x > .2f)
		this->v_x = .2f;

	if (this->v_x < -.2f)
		this->v_x = -.2f;

	// Vertical speed check (dead zone)
	if (this->v_y < 0.0005 && this->v_y > -0.0005)
		this->v_y = 0.0;

	// Horizontal speed check (dead zone)
	if (this->v_x < 0.0005 && this->v_x > -0.0005)
		this->v_x = 0.0;

	GameObject::update(deltaTime);

	UpdateAnimation(deltaTime);

	// Lower limit
	if (this->y + this->height > mapH)
	{
		this->y = mapH - this->height;
		this->v_y = 0.0f;
	}
	// Upper limit
	if (this->y < 0.0f)
	{
		this->y = 0;
		this->v_y = 0.0f;
	}

	// Left limit
	if (this->x < 0.0f)
	{
		this->x = 0;
		this->v_x = 0.0f;
	}
	// Right limit
	if (this->x + this->width > mapW)
	{
		this->x = (float)(mapW - this->width);
		this->v_x = 0.0f;
	}

	/*enemy logic here*/
	if (shootTimer < shootTimerMax)
		shootTimer += deltaTime;
	else if (shootTimer >= shootTimerMax)
	{
		shootTimer = 0;

		//avoid shooting with both at the same time
		heavyShootTimer -= 1;
		Shoot();
	}

	if (heavyShootTimer < heavyShootTimerMax)
		heavyShootTimer += deltaTime;
	else if (heavyShootTimer >= heavyShootTimerMax)
	{
		heavyShootTimer = 0;
		shootTimer -= 1;
		ShootHeavy();
	}

	jumpTimer += deltaTime;
	if (jumpTimer >= jumpTimerMax)
	{
		jump(deltaTime);
		//assigns random jump timer
		jumpTimer = rand() % ((int)jumpTimerMax - 1);
	}
	//std::cout << shootTimer << std::endl;

}

//enemy normal ammo shoot function 
void Enemy::Shoot()
{
	Player* p = m_game->GetPlayer();

	int p_x = p->getX();
	int p_y = p->getY() + (p->height / 2);

	// Create a new ammo and set its coords to match player position
	Ammo* newAmmo = new Ammo(GameValues::ammoColor, 3, AmmoType::normal);
	newAmmo->Reset(this->getX() + this->width / 2, this->getY() + this->height / 2);
	newAmmo->setGravity(false);

	// Push the ammo into the ammo vector
	this->m_game->vec_ammo.push_back(newAmmo);
	
	if (p->getX() > this->x)
		isFacingLeft = false;
	else
		isFacingLeft = true;

	// Set the aim coords for the ammo, little bit of randomization so enemy can miss
	newAmmo->setAimCoords(p_x + ((rand() % 50) - 25), p_y + ((rand() % 50) - 25));

	// Fire the ammo
	newAmmo->fire(false);
	m_game->GetSoundManager()->playSound("shoot1");
}

//shoot heavy ammo
void Enemy::ShootHeavy()
{
	Player* p = m_game->GetPlayer();

	int p_x = p->getX();
	int p_y = p->getY() + (p->height / 2);

	// Create a new ammo and set its coords to match player position
	HeavyAmmo* newAmmo = new HeavyAmmo(GameValues::heavyAmmoColor, 5, AmmoType::heavy, 100);
	newAmmo->Reset(this->getX() + this->width / 2, this->getY() + this->height / 2);

	// Push the ammo into the ammo vector
	this->m_game->vec_ammo.push_back(newAmmo);

	if (p->getX() > this->x)
		isFacingLeft = false;
	else
		isFacingLeft = true;

	//TODO: Calculate how high enemy needs to shoot in order to hit player
	newAmmo->setAimCoords(p_x + ((rand() % 50) - 25), p_y + ((rand() % 50) - 25));

	// Fire the ammo
	newAmmo->fire(false);
	m_game->GetSoundManager()->playSound("hShoot");
}

//is called when enemy takes dmg
void Enemy::TakeDmg(float dmg)
{
	if (m_game->restartPending())
		return;

	health -= dmg;
	ShowDmg(dmg);

	//check if we died from the dmg
	if (health <= 0)
	{
		health = 0;
		//DEAD
		m_game->setPlayerScore(m_game->getPlayerScore() + 1);

		//start restart timer
		m_game->RestartGame();
		m_game->GetSoundManager()->playSound("dead");
		isDead = true;
	}
}

//updates animation logic of enemy
void Enemy::UpdateAnimation(float deltaTime)
{
	//std::cout << v_x << std::endl;
	if (m_game->restartPending())
	{
		if (isDead)
			deadAnim.update(deltaTime);
		return;
	}

	if (abs(v_y) > 0.05f)
		jumpAnim.update(deltaTime);
	else if (abs(v_x) > 0.1f)
		runAnim.update(deltaTime);
	else
		idleAnim.update(deltaTime);
}

//jump function for enemy, we also add some x velocity so it looks better
void Enemy::jump(float deltaTime)
{
	if (m_game->restartPending())
		return;

	//this->y -= GameValues::playerJump * deltaTime;
	this->v_y = 0;
	this->v_y -= GameValues::playerJump * deltaTime;

	//TODO: Make global values for this
	this->v_x += ((rand() % 2) == 0) ? -300 * deltaTime : 300 * deltaTime;
}