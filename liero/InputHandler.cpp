#include "InputHandler.h"
#include "Game.h"

InputHandler::InputHandler(sf::RenderWindow* window, Game* game)
{
	this->window = window;
	this->game = game;
}


InputHandler::~InputHandler()
{
}

//process player input
void InputHandler::ProcessInput(float deltaTime)
{
	sf::Event event;
	while (window->pollEvent(event))
	{
		/*close window event*/
		if (event.type == sf::Event::Closed)
			window->close();

		//either quit the game or go back to menu, based on our current state
		if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Escape)
		{
			if (game->gState == Game::GameState::game)
			{
				game->Pause();
			}
			else if (game->gState == Game::GameState::pauseMenu)
			{
				game->GoToMenu();
			}
			else if (game->gState == Game::GameState::menu)
			{
				window->close();
			}
		}
			
		//enter to game if we are in menu or pause menu
		if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Enter && game->gState == Game::GameState::menu ||
			event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Enter && game->gState == Game::GameState::pauseMenu)
		{
			game->Continue();
		}
			

		//only check for game input while game is active
		if (game->GameIsActive && !game->GameIsPaused)
		{
			//Update mouse coordinates
			if (event.type == sf::Event::MouseMoved)
			{
				game->GetPlayer()->aim_x = event.mouseMove.x;
				game->GetPlayer()->aim_y = event.mouseMove.y;
			}

			// If the mouse button was pressed, fire the ammo
			if (event.type == sf::Event::MouseButtonPressed && !game->restartPending())
			{
				if (event.mouseButton.button == sf::Mouse::Left && game->GetPlayer()->getCanShoot())
				{
					game->GetPlayer()->Shoot(AmmoType::normal, game->vec_ammo, event.mouseButton.x, event.mouseButton.y);
				}
				else if (event.mouseButton.button == sf::Mouse::Right && game->GetPlayer()->getHeavyCanShoot())
				{
					game->GetPlayer()->Shoot(AmmoType::heavy, game->vec_ammo, event.mouseButton.x, event.mouseButton.y);
				}
			}

			/*if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::G)
				game->getPlayer()->setGravity(!game->getPlayer()->getGravity());*/

			if (event.type == sf::Event::KeyPressed && event.key.code == sf::Keyboard::Space && *game->GetPlayer()->pGetCanJump() && *game->GetPlayer()->pGetJumpCount() > 0)
				game->GetPlayer()->jump(deltaTime);

			if (event.type == sf::Event::KeyPressed)
			{
				switch (event.key.code)
				{
				case sf::Keyboard::A:
					this->playerMoveLeft = true;
					break;
				case sf::Keyboard::S:
					this->playerCrouch = true;
					game->GetPlayer()->PlayerCrouch();
					break;
				case sf::Keyboard::D:
					this->playerMoveRight = true;
					break;
				default:
					break;
				}
			}

			if (event.type == sf::Event::KeyReleased)
			{
				switch (event.key.code)
				{
				case sf::Keyboard::A:
					this->playerMoveLeft = false;
					break;
				case sf::Keyboard::S:
					this->playerCrouch = false;
					game->GetPlayer()->resetHeight();
					break;
				case sf::Keyboard::D:
					this->playerMoveRight = false;
					break;
				default:
					break;
				}
			}

			game->GetPlayer()->moveLeft = playerMoveLeft;
			game->GetPlayer()->moveRight = playerMoveRight;

			/*player movement*/
			/*if (event.type == sf::Event::KeyPressed)
			{
				if (playerMoveLeft)
					game->GetPlayer()->MoveLeft(deltaTime);
				if (playerCrouch)
					game->GetPlayer()->PlayerCrouch();
				if (playerMoveRight)
					game->GetPlayer()->MoveRight(deltaTime);
			}*/
		}
	}
}
