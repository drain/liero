#include "HeavyAmmo.h"

HeavyAmmo::HeavyAmmo(sf::Color col, float r, AmmoType type, float gMultiplier) : Ammo(col, r, type)
{
	this->gravityMultiplier = gMultiplier;
}

void HeavyAmmo::fire(bool playerShot)
{
	this->playerShot = playerShot;
	// set the velocity of the ammo correctly
	double xdiff = this->aim_x - this->x;
	double ydiff = this->aim_y - this->y;

	double hyp = sqrt(xdiff*xdiff + ydiff * ydiff);
	// the total velocity is 1.0
	this->v_x = GameValues::heavyAmmoSpeed * xdiff / hyp;
	this->v_y = GameValues::heavyAmmoSpeed * ydiff / hyp;

	this->useGravity = true;

	// set this ammo LIVE!!!
	this->bLive = true;
}


HeavyAmmo::~HeavyAmmo()
{
}
