#include "Player.h"
#include "Game.h"


Player::Player(Game* game)
{
	m_game = game;
	this->x = 0;
	this->y = 0;
	this->moveSpeed = GameValues::playerSpeed;
	this->jumpSpeed = GameValues::playerJump;
	this->maxHealth = GameValues::playerMaxHealth;
	this->shootTimerMax = GameValues::playerShootDelay;
	this->heavyShootTimerMax = GameValues::playerHeavyShootDelay;

	this->health = this->maxHealth;
	this->jumpCount = this->jumpCountMax;
	
	// Load the image...
	playerText.loadFromFile("sprites/soldier.png");
	playerSprite.setTexture(playerText);

	crosshairText.loadFromFile("sprites/crosshair.png");
	crosshairSprite.setTexture(crosshairText);
	crosshairSprite.setScale(.05f, .05f);

	//setup animations
	idleAnim.setTarget(&playerSprite);
	idleAnim.addFrame({ sf::IntRect(0,2,31,48), 0.4 });
	idleAnim.addFrame({ sf::IntRect(0,2,31,48), 0.4 });
	idleAnim.playbackMode = PlaybackMode::Loop;

	jumpAnim.setTarget(&playerSprite);
	jumpAnim.addFrame({ sf::IntRect(205,2,31,48), 0.4 });
	jumpAnim.addFrame({ sf::IntRect(205,2,31,48), 0.4 });

	runAnim.setTarget(&playerSprite);
	runAnim.addFrame({ sf::IntRect(0,58,31,48), 0.1 });
	runAnim.addFrame({ sf::IntRect(54,58,31,48), 0.1 });
	runAnim.addFrame({ sf::IntRect(102,58,31,48), 0.1 });
	runAnim.addFrame({ sf::IntRect(150,58,31,48), 0.1 });
	runAnim.addFrame({ sf::IntRect(258,58,31,48), 0.1 });
	runAnim.addFrame({ sf::IntRect(307,58,31,48), 0.1 });
	runAnim.addFrame({ sf::IntRect(353,58,31,48), 0.1 });
	runAnim.playbackMode = PlaybackMode::Loop;

	crouchAnim.setTarget(&playerSprite);
	crouchAnim.addFrame({ sf::IntRect(100,2,31,48), 0.4 });
	crouchAnim.addFrame({ sf::IntRect(100,2,31,48), 0.4 });
	crouchAnim.changeOrigin = false;

	deadAnim.setTarget(&playerSprite);
	deadAnim.addFrame({ sf::IntRect(295,2,45,48), 1 });
	deadAnim.addFrame({ sf::IntRect(295,2,45,48), 1 });
}


Player::~Player()
{
}

void Player::SetPosition(int x, int y)
{
	this->x = x;
	this->y = y;
}

void Player::SetSpeed(sf::Vector2f speed)
{
	this->v_x = speed.x;
	this->v_y = speed.y;
}

//update player logic
void Player::Update(int mapW, int mapH, float deltaTime)
{
	//dont update logic if restart is pending
	if (m_game->restartPending())
		return;

	if (moveLeft)
		MoveLeft(deltaTime);
	if (moveRight)
		MoveRight(deltaTime);

	shootTimer += deltaTime;
	if (shootTimer >= shootTimerMax)
	{
		canShoot = true;
		shootTimer = shootTimerMax;
	}

	heavyShootTimer += deltaTime;
	if (heavyShootTimer >= heavyShootTimerMax)
	{
		heavyCanShoot = true;
		heavyShootTimer = heavyShootTimerMax;
	}

	//update player dmg numbers
	UpdateShowDmg();

	//if grounded, slow our X speed if speed is not applied
	if (this->v_x > 0 && isGrounded)
		this->v_x -= moveSpeed * .5f * deltaTime;
	else if(this->v_x < 0 && isGrounded)
		this->v_x += moveSpeed * .5f * deltaTime;

	this->x += this->v_x;
	this->y += this->v_y;

	// Horizontal speed check:
	if (this->v_x > GameValues::playerSpeed)
		this->v_x = GameValues::playerSpeed;

	if (this->v_x < -GameValues::playerSpeed)
		this->v_x = -GameValues::playerSpeed;

	// Vertical speed check (dead zone)
	if (this->v_y < 0.0005 && this->v_y > -0.0005)
		this->v_y = 0.0;

	// Horizontal speed check (dead zone)
	if (this->v_x < 0.0005 && this->v_x > -0.0005)
		this->v_x = 0.0;


	GameObject::update(deltaTime);

	UpdateAnimation(deltaTime);

	// Lower limit
	if (this->y + this->height > mapH) 
	{
		this->y = mapH - this->height;
		this->v_y = 0.0f;
	}
	// Upper limit
	if (this->y < 0.0f) 
	{
		this->y = 0;
		this->v_y = 0.0f;
	}

	// Left limit
	if (this->x < 0.0f) 
	{
		this->x = 0;
		this->v_x = 0.0f;
	}
	// Right limit
	if (this->x + this->width > mapW) 
	{
		this->x = (float)(mapW - this->width);
		this->v_x = 0.0f;
	}
}

//do playe crouch, lower players height
void Player::PlayerCrouch()
{
	if (m_game->restartPending())
		return;

	height = maxHeight - 10;
	if (!crouch)
	{
		this->playerSprite.setOrigin(this->playerSprite.getOrigin().x, this->playerSprite.getOrigin().y + 10);
		this->SetPosition(this->x, this->y + 10);
	}
		
	crouch = true;
}

//reset player height (after crouch)
void Player::resetHeight()
{
	if (m_game->restartPending())
		return;

	//incase we are at the top of the map and uncrouch, otherwise we go out of bounds and crash the game
	if ((this->y - 10) < 0)
	{
		this->SetPosition(this->x, this->y + 10);
	}

	this->height = this->maxHeight;
	this->crouch = false;
	this->SetPosition(this->x, this->y - 10);
}

//reset player values
void Player::Reset()
{
	if (isCrouched())
		resetHeight();

	isDead = false;
	canShoot = false;
	heavyCanShoot = false;
	isDead = false;
	showDmg = false;
	setHealth(getMaxHealth());
	v_x = 0;
	v_y = 0;
	jumpCount = 0;

	shootTimer = 0;
	heavyShootTimer = 0;
}

//trigger player dmg text
void Player::ShowDmg(float dmg)
{
	lastDmgTaken = dmg;
	showDmg = true;
	dmgPos = sf::Vector2f(this->getX() + (rand() % 40 - 20), this->getY() + (rand() % 40 - 20));
}

//update player dmg text timer
void Player::UpdateShowDmg()
{
	if (!showDmg)
		return;

	if (dmgDrawTimer <= dmgDrawTimerMax)
		dmgDrawTimer += GameValues::PHYSICS_TIME_STEP.asSeconds();
	else if (dmgDrawTimer > dmgDrawTimerMax)
	{
		showDmg = false;
		dmgDrawTimer = 0;
	}
}

void Player::TakeDmg(float dmg)
{
	//if game ended stop taking dmg
	if (m_game->restartPending())
		return;

	health -= dmg;

	//draw dmg to screen
	ShowDmg(dmg);

	if (health <= 0)
	{
		health = 0;
		//DEAD
		m_game->setEnemyScore(m_game->getEnemyScore() + 1);
		//if player dies, call end round
		m_game->RestartGame();
		m_game->GetSoundManager()->playSound("dead");
		this->isDead = true;
	}
}

//update player animation logic
void Player::UpdateAnimation(float deltaTime)
{
	if (m_game->restartPending())
	{
		if (isDead)
			deadAnim.update(deltaTime);
		return;
	}

	//check which anim to play, anims are in priority order
	
	if (crouch)
		crouchAnim.update(deltaTime);
	else if (abs(v_y) > 0.05f)
		jumpAnim.update(deltaTime);
	else if (abs(v_x) > 0.1f)
		runAnim.update(deltaTime);
	else
		idleAnim.update(deltaTime);
	
}

//player jump, reduces jump count and increase our y velocity
void Player::jump(float deltaTime)
{
	if (m_game->restartPending())
		return;

	this->v_y = 0;
	this->v_y -= GameValues::playerJump * deltaTime;

	sm->playSound("jump");
	jumpCount--;
	if(jumpCount <= 0)
		canJump = false;
}

//player shoot, shoot different ammo based on type, 
//pass reference to ammo vector from game class here, so we dont have to include whole game class
void Player::Shoot(AmmoType type, std::vector<Ammo*> &vec_ammo, int x, int y)
{
	if (type == AmmoType::normal)
	{
		//cant shoot with both weapons at the same time
		if (heavyCanShoot)
		{
			heavyShootTimer -= GameValues::playerShootDelay;
			heavyCanShoot = false;
		}

		canShoot = false;
		shootTimer = 0;

		// Create a new ammo and set its coords to match player position
		Ammo* newAmmo = new Ammo(GameValues::ammoColor, 3, AmmoType::normal);
		newAmmo->Reset(getX() + width / 2, getY() + height / 2);
		newAmmo->setGravity(false);

		// Push the ammo into the ammo vector
		vec_ammo.push_back(newAmmo);

		// Set the aim coords for the ammo
		newAmmo->setAimCoords(x, y);

		// Fire the ammo
		newAmmo->fire(true);
		sm->playSound("shoot1");
	}
	else if (type == AmmoType::heavy)
	{
		//cant shoot with both weapons at the same time
		if (canShoot)
		{
			shootTimer -= GameValues::playerShootDelay;
			canShoot = false;
		}

		heavyCanShoot = false;
		heavyShootTimer = 0;

		// Create a new ammo and set its coords to match player position
		HeavyAmmo* newAmmo = new HeavyAmmo(GameValues::heavyAmmoColor, 5, AmmoType::heavy, 100);
		newAmmo->Reset(getX() + width / 2, getY() + height / 2);

		// Push the ammo into the ammo vector
		vec_ammo.push_back(newAmmo);

		// Set the aim coords for the ammo
		newAmmo->setAimCoords(x, y);

		// Fire the ammo
		newAmmo->fire(true);
		sm->playSound("hShoot");
	}
	
}
