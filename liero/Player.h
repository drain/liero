#pragma once
#include "SFML/Graphics.hpp"
#include "list"
#include "Ammo.h"
#include "SoundManager.h"
#include "Animation.h"
#include "vector"

class Game;

//TODO: Add Entity class that has some of the implementation of player and enemy entities, outside of input and AI logic
//most of the code in player and enemy are the same, so that would make perfect sense :)

class Player : public GameObject
{
private:
	Game* m_game;

	float moveSpeed;
	float jumpSpeed;

	float health;
	float maxHealth;

	SoundManager* sm;
	sf::Texture playerText;
	sf::Sprite playerSprite;

	sf::Texture crosshairText;
	sf::Sprite crosshairSprite;

	Animation idleAnim;
	Animation jumpAnim;
	Animation runAnim;
	Animation crouchAnim;
	Animation deadAnim;

	bool animIsPlaying;
	bool playJumpAnim;

	bool isFacingLeft = false;
	bool isGrounded = false;

	bool crouch = false;

	bool canJump = false;
	int jumpCount;
	int jumpCountMax = 2;

	float shootTimer;
	float shootTimerMax;
	bool canShoot = false;

	float heavyShootTimer;
	float heavyShootTimerMax;
	bool heavyCanShoot;

	bool isDead = false;

	float dmgDrawTimer = 0;
	float dmgDrawTimerMax = 1;
	bool showDmg = false;
	sf::Vector2f dmgPos;
	
	float lastDmgTaken;
	void UpdateShowDmg();

public:
	Player(Game* game);
	~Player();

	int getX() { return this->x; };
	int getY() { return this->y; };

	void MoveLeft(float deltaTime) { if(this->v_x > 0) this->v_x = 0; this->v_x -= moveSpeed * deltaTime; };
	void MoveRight(float deltaTime) { if (this->v_x < 0) this->v_x = 0; this->v_x += moveSpeed * deltaTime; };
	void PlayerCrouch();

	bool moveLeft = false;
	bool moveRight = false;

	void SetPosition(int x, int y);

	void SetSpeed(sf::Vector2f speed);
	sf::Vector2f getSpeed() { return sf::Vector2f(this->v_x, this->v_y); };

	int width = 20;
	int height = 40;
	int maxHeight = 40;

	void Update(int mapW, int mapH, float deltaTime);

	float getHealth() { return this->health; };
	float getMaxHealth() { return this->maxHealth; };
	void setHealth(float health) { this->health = health; };

	void TakeDmg(float dmg);

	void UpdateAnimation(float deltaTime);

	//bool useGravity = false;

	// aim target
	double aim_x, aim_y;

	// ammo list
	std::list<Ammo> listAmmo;

	void jump(float deltaTime);

	void Shoot(AmmoType type, std::vector<Ammo*> &vec_ammo, int x, int y);

	void resetHeight();

	void Reset();

	void ShowDmg(float dmg);

	void setSM(SoundManager* sm) { this->sm = sm; };

	sf::Sprite* pGetPlayerSprite() { return &playerSprite; };

	sf::Sprite* pGetCrosshairSprite() { return &crosshairSprite; };

	/*TODO: Make getters and setters instead of pointers*/

	bool isCrouched() { return this->crouch; };

	float getLastDmgTaken() { return this->lastDmgTaken; };

	sf::Vector2f getDmgPos() { return this->dmgPos; };

	bool* pGetFacingLeft(){ return &isFacingLeft; };

	bool* pGetIsGrounded() { return &isGrounded; };

	bool* pGetCanJump() { return &canJump; };

	int* pGetJumpCount() { return &jumpCount; };

	int getJumpCountMax() { return jumpCountMax; };

	bool getCanShoot() { return canShoot; };

	bool getHeavyCanShoot() { return heavyCanShoot; };

	bool getShowDmg() { return showDmg; };

	//bool* pGetIsDead() { return &isDead; };
	//float* getShootTimer() { return &shootTimer; };
	//float* getHeavyShootTimer() { return &heavyShootTimer; };
};

