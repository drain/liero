#pragma once

#include <stdio.h>
#include "GameObject.h"
#include "Map.h"
#include "GameValues.h"

enum AmmoType
{
	normal,
	heavy
};

class Ammo : public GameObject 
{
public:

	Ammo(sf::Color col, float r, AmmoType type);

	AmmoType type;

	void setAimCoords(double x, double y) 
	{
		this->aim_x = x;
		this->aim_y = y;
	};

	bool isLive() { return this->bLive; };

	// Let's overload the reset()-method
	void Reset(double x, double y);

	virtual void fire(bool playerShot);

	void update(float deltaTime);

	// Checks collision between this ammo and the level given
	sf::Color checkCollisionWithLevel(Map map);

	// Checks if ammo is out of bounds
	bool checkOutOfBounds(Map map);

	// Returns the shape to be drawn
	sf::CircleShape getShape() { return this->shape; };

	bool playerShot = false;

protected:

	sf::CircleShape shape;

	double aim_x;
	double aim_y;

	// Is the ammo live or not?
	bool bLive = false;
};
