#pragma once
#include "SFML/Graphics.hpp"
#include <cstdlib>
#include <stdlib.h>
#include <time.h>
#include "string"
#include "sstream"
#include "iomanip"
#include "ParticleSystemWithTexture.h"

class Game;

class Renderer
{
private:
	int screenWidth;
	int screenHeight;
	sf::RenderWindow* window = nullptr;
	Game* m_game = nullptr;
	sf::Font arial;

	float fpsUpdateTimer = 0;
	float fpsPerFrame = 0;
	float frameCounter;
public:
	Renderer(int w, int h, Game* game);
	~Renderer();
	sf::RenderWindow* getWindow();
	void DrawWindow();

	void generateBlock(sf::Color col);
	void generateBlock(sf::Color col, int x, int y, int w, int h);
	void generateCircle(sf::Color col);
	void generateCircle(sf::Color col, int x, int y, int r);

	void DrawPlayer(sf::Color col, int x, int y);
	void DrawEnemy(sf::Color col, int x, int y);
	void DisplayFPS(float deltaTime);

	void DrawScore(std::string text, sf::Color col);
	void DrawGameEndText(std::string text, sf::Color col);

	void DrawHealthBar(sf::Color col, int x, int y);
	void DrawAmmoCds(sf::Color col, int x, int y);

	void DrawHealthBarEnemy(sf::Color col, int x, int y);

	void DrawDmgTaken(float dmg, int x, int y);

	void DrawPauseMenu();
};

