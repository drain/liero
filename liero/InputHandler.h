#pragma once
#include "SFML/Graphics.hpp"
#include "Ammo.h"
#include "HeavyAmmo.h"
#include "SoundManager.h"

class Game;

class InputHandler
{
private:
	sf::RenderWindow* window = nullptr;
	Game* game = nullptr;

	bool playerMoveUp = false;

	bool playerCrouch = false;
	bool playerMoveLeft = false;
	bool playerMoveRight = false;

public:
	InputHandler(sf::RenderWindow* window, Game* game);
	~InputHandler();

	void ProcessInput(float deltaTime);
};

