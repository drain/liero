#include "SFML/Graphics.hpp"
#include "Game.h"
#include <cstdlib>
#include <stdlib.h>
#include <time.h> 

int main()
{
	srand(time(NULL));

	//TODO:: resolution selection
	Game game(800, 800);

	game.Start();

	return 0;
}