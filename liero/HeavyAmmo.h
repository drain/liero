#pragma once
#include "Ammo.h"

class HeavyAmmo : public Ammo
{
public:
	HeavyAmmo(sf::Color col, float r, AmmoType type, float gMultiplier);
	void fire(bool playerShot);
	~HeavyAmmo();
};

