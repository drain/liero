#include "GameValues.h"

//static values that can be used to change game aspects fast for testing

sf::Color GameValues::emptyColor = sf::Color(216, 188, 162);
sf::Color GameValues::unbreakAbleColor = sf::Color(48, 46, 45);
sf::Color GameValues::breakAbleColor = sf::Color(193, 119, 50);
sf::Color GameValues::ammoColor = sf::Color(100,100,100);
sf::Color GameValues::heavyAmmoColor = sf::Color(255, 56, 56);

sf::Color GameValues::playerColor = sf::Color(100, 100, 100);
sf::Color GameValues::enemyColor = sf::Color(100, 100, 100);

float GameValues::winCondition = 5;

float GameValues::gravity = 1;

float GameValues::playerSpeed = 1;
float GameValues::playerJump = 85;
float GameValues::playerMaxHealth = 100;

float GameValues::enemyMaxHealth = 100;

float GameValues::explosionRadius = 30;
float GameValues::heavyAmmoRadius = 75;

float GameValues::ammoSpeed = 350;
float GameValues::heavyAmmoSpeed = 225;

float GameValues::playerShootDelay = .75f;
float GameValues::enemyShootDelay = 2;

float GameValues::playerHeavyShootDelay = 3;
float GameValues::enemyHeavyShootDelay = 5;

float GameValues::blockAmount = 75;
float GameValues::minBlockW = 30;

//refresh rate of the game physics
sf::Time GameValues::PHYSICS_TIME_STEP = sf::seconds(1.f / 60.f);