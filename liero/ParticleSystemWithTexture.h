//
//  ParticleSystemWithTexture.hpp
//  Liero
//
//  Created by Teemu Saarelainen on 16/04/2019.
//  Copyright � 2019 Teemu Saarelainen. All rights reserved.
//
#pragma once

#include <stdio.h>

#include <SFML/Graphics.hpp>
#include <cmath>
#include <iostream>

class ParticleSystemWithTexture : public sf::Drawable, public sf::Transformable
{
public:

	ParticleSystemWithTexture(unsigned int count) :
		m_particles(count),
		m_vertices(sf::Quads, count * 4),    // These are now quads! multiply by 4
		m_lifetime(sf::milliseconds(500)),
		m_emitter(0, 0),
		m_direction(0),
		m_spread(0),
		m_speed(50),
		m_color(sf::Color::White),
		m_base_quad(sf::Quads, 4),   // Do a base quad for a single particle / quad in the vertex array
		m_texture()  // We need a texture for this, construct an empty one
	{
		m_base_quad[0].position = sf::Vector2f(-0.5, -0.5);
		m_base_quad[1].position = sf::Vector2f(0.5, -0.5);
		m_base_quad[2].position = sf::Vector2f(0.5, 0.5);
		m_base_quad[3].position = sf::Vector2f(-0.5, 0.5);

		// No need to do texture coords at this point...

	}

	void setTexture(const sf::Texture &texture) {
		m_texture = sf::Texture(texture);
	}

	void setBaseQuad(sf::VertexArray arr) {
		if (arr.getVertexCount() < 4)
			return;
		for (int i = 0; i < 4; i++) {
			m_base_quad[i].position = arr[i].position;
			m_base_quad[i].texCoords = arr[i].texCoords;
		}
	}

	void setLifetime(unsigned millis)
	{
		m_lifetime = sf::milliseconds(millis);
	}

	void setEmitter(sf::Vector2f position)
	{
		m_emitter = position;
	}

	void setDirection(float direction) {
		m_direction = direction;
	}

	void setSpread(float spread) {
		m_spread = spread;
	}

	void setSpeed(float speed) {
		m_speed = speed;
	}


	void setColor(sf::Color color) {
		m_color = color;
	}


	void resetParticles() {
		for (std::size_t i = 0; i < m_particles.size(); ++i)
				resetParticle(i);
	}

	void update(sf::Time elapsed)
	{
		// Update each particle - remember, each particle has 4 vertices now!
		for (std::size_t i = 0; i < m_particles.size(); ++i)
		{
			// update the particle lifetime
			Particle& p = m_particles[i];
			p.lifetime -= elapsed;

			// if the particle is dead, respawn it
			/*
			 if (p.lifetime <= sf::Time::Zero)
			 resetParticle(i);
			 */

			 // If the particle is dead make it disappear
			 /*
			 if (p.lifetime <= sf::Time::Zero){
				 for (int v=0;v<4;v++)
					 m_vertices[i*4+v].color.a = 0;
			 }
			 */

			 // update the position of each corresponding vertex
			for (int v = 0; v < 4; v++)
				m_vertices[i * 4 + v].position += p.velocity * elapsed.asSeconds();

			// update the alpha (transparency) of the particle according to its lifetime
			double ratio = p.lifetime.asMilliseconds() / static_cast<double>(m_lifetime.asMilliseconds());

			if (ratio > 0.0) {
				for (int v = 0; v < 4; v++)
					m_vertices[i * 4 + v].color.a = static_cast<sf::Uint8>(ratio * 255.0);
			}
			else {
				for (int v = 0; v < 4; v++)
					m_vertices[i * 4 + v].color.a = 0;
			}


		}
	}

private:

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const
	{
		// apply the transform
		states.transform *= getTransform();

		// apply the texture
		states.texture = &m_texture;

		// draw the vertex array
		target.draw(m_vertices, states);

		//window->draw(m_vertices);
	}

private:

	struct Particle
	{
		sf::Vector2f velocity;
		sf::Time lifetime;
	};

	void resetParticle(std::size_t index)
	{
		// give a random velocity and lifetime to the particle
		//        float angle = (std::rand() % 360) * 3.14f / 180.f;
		//std::cout << "direction= "  << m_direction << ", ";
		float angle = (m_direction) * 3.14f / 180.f;
		//std::cout << "--> angle= "  << angle << ", ";
		angle += ((std::rand() % (int)m_spread) - m_spread / 2.0f) * 3.14f / 180.f;
		//std::cout << "final angle= "  << angle << std::endl;
		float speed = m_speed + std::rand() % static_cast<int>(m_speed*0.25);
		m_particles[index].velocity = sf::Vector2f(std::cos(angle) * speed, std::sin(angle) * speed);

		//m_particles[index].lifetime = sf::milliseconds((std::rand() % 500) + 500);
		m_particles[index].lifetime = m_lifetime;

		// reset the position of the corresponding vertex
		for (int v = 0; v < 4; v++) {
			m_vertices[index * 4 + v].position = m_base_quad[v].position + m_emitter;
			m_vertices[index * 4 + v].texCoords = m_base_quad[v].texCoords;
			// reset the color
			m_vertices[index * 4 + v].color = m_color;
		}
	}

	std::vector<Particle> m_particles;
	sf::VertexArray m_vertices;
	sf::Time m_lifetime;
	sf::Vector2f m_emitter;
	float m_direction;
	float m_spread;
	float m_speed;
	sf::Color m_color;
	sf::VertexArray m_base_quad;
	sf::Texture m_texture;
};
