#include "SoundManager.h"

SoundManager::SoundManager()
{
	preloadSounds();
}

void SoundManager::preloadSounds()
{
	/*load sounds here*/
	SoundAsset explosion = SoundAsset("explosion", "sounds/explosion.wav");
	explosion.sound.setVolume(10);
	sounds.push_back(explosion);

	sounds.push_back(SoundAsset("dead", "sounds/dead.wav"));

	SoundAsset jump = SoundAsset("jump", "sounds/jump.wav");
	jump.sound.setVolume(10);
	sounds.push_back(jump);

	sounds.push_back(SoundAsset("hit", "sounds/hit.wav"));

	SoundAsset shoot1 = SoundAsset("shoot1", "sounds/shoot1.wav");
	shoot1.sound.setVolume(10);
	sounds.push_back(shoot1);

	sounds.push_back(SoundAsset("shoot2", "sounds/shoot2.wav"));

	SoundAsset hShoot = SoundAsset("hShoot", "sounds/hShoot.wav");
	hShoot.sound.setVolume(50);
	sounds.push_back(hShoot);
	
	SoundAsset hExplosion = SoundAsset("hExplosion", "sounds/hExplosion.wav");
	hExplosion.sound.setVolume(100);
	sounds.push_back(hExplosion);
}

//play sound based on name
void SoundManager::playSound(std::string name)
{
	for(int i = 0; i < sounds.size();i++)
		if (sounds[i].name == name)
		{
			sounds[i].play();
			break;
		}
}

//TODO: Edit this so we can play multiple musicfiles
//loop background music
void SoundManager::playMusic()
{
	if (!music.openFromFile("sounds/8bit.wav"))
		std::cout << "ERROR LOADING MUSIC!" << std::endl;
	music.setLoop(true);
	music.setVolume(2.5f);
	music.setLoopPoints(sf::Music::TimeSpan::Span(sf::Time::Zero, music.getDuration() - sf::milliseconds(1200)));
	//music.setPitch(0.75f);

	music.play();
}

