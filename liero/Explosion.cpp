#include "Explosion.h"
#include "Game.h"

Explosion::Explosion(int radius, Game* game, float dmg) 
{
	// Create the explosion data
	this->m_game = game;
	this->radius = radius;
	this->dmg = dmg;

	explosion_data.create(radius * 2, radius * 2, sf::Color::Black);
	for (int x = 0; x < explosion_data.getSize().x; x++) {
		for (int y = 0; y < explosion_data.getSize().y; y++) {
			if ((x - radius)*(x - radius) + (y - radius)*(y - radius) <= radius * radius) {
				explosion_data.setPixel(x, y, sf::Color::White);
			}
			else {
				explosion_data.setPixel(x, y, sf::Color(0, 0, 0, 0));
			}
		}
	}
}

void Explosion::explode(sf::Image* pLeveldata, int screenW, int screenH, bool playerShot) 
{
	Player* player = m_game->GetPlayer();
	Enemy* enemy = m_game->GetEnemy();

	for (int x = 0; x < this->explosion_data.getSize().x; x++) 
	{
		for (int y = 0; y < this->explosion_data.getSize().y; y++) 
		{
			// If explosion mask and level image have both white,
			// then "erase" the level image pixel

			//sanity check for pixels
			if (this->getX() + x >= screenW || this->getX() + x < 0 || y + this->getY() >= screenH || y + this->getY() < 0)
				continue;

			if (this->explosion_data.getPixel(x, y) == sf::Color::White) {
				//debug purpose pLeveldata->setPixel(this->getX() + x, this->getY() + y, sf::Color(135, 206, 235));
				if (pLeveldata->getPixel(this->getX() + x, this->getY() + y) == GameValues::breakAbleColor) {
					pLeveldata->setPixel(this->getX() + x, this->getY() + y, GameValues::emptyColor);
				}
			}
		}
	}

	//calculate middle points for explosion, player and enemy
	//TODO: Maybe it would be better to have the game logic revolver around that point being in middle all the time?
	float ex_cX = x + explosion_data.getSize().x / 2;
	float ex_cY = y + explosion_data.getSize().y / 2;

	float p_cX = player->getX() + player->width / 2;
	float p_cY = player->getY() + player->height / 2;

	float e_cX = enemy->getX() + enemy->width / 2;
	float e_cY = enemy->getY() + enemy->height / 2;
	
	/*check if explosion hit either player or enemy*/
	float distanceToPlayer = pow(ex_cX - p_cX, 2) + pow(ex_cY - p_cY, 2);
	float distanceToEnemy = pow(ex_cX - e_cX, 2) + pow(ex_cY - e_cY, 2);

	//debug data
	/*std::cout << "Player: " << p_cX << ", " << p_cY << std::endl;
	std::cout << "Explosion: " << ex_cX << ", " << ex_cY << std::endl;
	std::cout << "Distance: " << distanceToPlayer << std::endl;
	std::cout << "Explosion radius: " << radius*radius << std::endl;
	std::cout << std::endl;*/

	if (distanceToPlayer <= radius*radius)
	{
		//player is smarter (maybe?) so he takes dmg from own shots
		player->TakeDmg(dmg);
	}

	if (distanceToEnemy <= radius*radius)
	{
		//enemy doesnt take dmg from its own shots untill we make it smarter
		//std::cout << playerShot << std::endl;
		if(playerShot)
			enemy->TakeDmg(dmg);
	}

	bExploded = true;
}
