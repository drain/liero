#pragma once

#include <stdio.h>
#include <SFML/Graphics.hpp>
#include "GameObject.h"
#include "GameValues.h"

class Game;

class Explosion : public GameObject 
{
public:

	Explosion(int radius, Game* game, float dmg);
	Explosion() {};

	sf::Image explosion_data;  // For the explosion effect

	void explode(sf::Image* level, int screenW, int screenH, bool playerShot);
	void reArm() { bExploded = false; };
	bool hasExploded() { return bExploded; };

	bool isVisible() { return bVisible; };

	int radius;
	float dmg;

private:
	Game* m_game;
	bool bExploded = false;
	bool bVisible = false;

};
