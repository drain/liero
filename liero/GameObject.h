#pragma once

#include <stdio.h>
#include <SFML/Graphics.hpp>
#include "GameValues.h"

class GameObject 
{

public:
	int getX() { return this->x; };
	int getY() { return this->y; };

	void SetPosition(double x, double y) {
		this->x = x;
		this->y = y;
	};

	void setGravity(bool g) { this->useGravity = g; };
	bool getGravity() { return this->useGravity; };

	void setSpeed(sf::Vector2f v2f_speed) {
		this->v_x = v2f_speed.x;
		this->v_y = v2f_speed.y;
	};

	sf::Vector2f getSpeed() { return sf::Vector2f(this->v_x, this->v_y); };

	void update(float deltaTime) {
		// Gravity effect
		if (this->useGravity)
			this->v_y += GameValues::gravity * gravityMultiplier * deltaTime;
	};

protected:
	float x = 0;
	float y = 0;

	float v_x = 0;
	float v_y = 0;

	bool useGravity = true;
	float gravityMultiplier = 1;

};
