#pragma once
#include "Map.h"
#include "Renderer.h"
#include "InputHandler.h"
#include "Player.h"
#include "Explosion.h"
#include "GameValues.h"
#include "Enemy.h"
#include "SoundManager.h"
#include "ParticleSystemWithTexture.h"


class Game
{
private:
	Renderer* rend;
	Map* gameMap;
	Player* player;
	Enemy* enemy;
	InputHandler* inputHandler;
	SoundManager* soundManager;

	ParticleSystemWithTexture* smallAmmoParticle;
	ParticleSystemWithTexture* heavyAmmoParticle;
	sf::Texture explosionText;

	sf::Clock clock;
	float elapsed;

	void UpdatePlayerCollisions();
	void UpdateEnemyCollisions();

	bool PlayerCheckCollisionBelow();
	bool PlayerCheckCollisionAbove();
	bool PlayerCheckCollisionRight();
	bool PlayerCheckCollisionLeft();

	bool EnemyCheckCollisionBelow(Enemy* enemy);
	bool EnemyCheckCollisionAbove(Enemy* enemy);
	bool EnemyCheckCollisionRight(Enemy* enemy);
	bool EnemyCheckCollisionLeft(Enemy* enemy);

	void AmmoCollided(Ammo* ammo);
	void CheckAmmoCollisions(float deltaTime);

	int playerScore, enemyScore;

	bool restart = false;
	bool playerWon = false;
	bool enemyWon = false;
	float restartTimer = 0;
	float restartTimerMax = 3;

	float shakeTimer = 0;
	float maxShakeTimer = 0.5f;
	float sPower = 1;
	bool shakeScreen = false;
	void UpdateScreenShake(float deltaTime);

	void ReloadGame();
	sf::Texture mainMenuText;
	sf::Sprite mainMenuSprite;
	
public:
	enum GameState
	{
		menu,
		pauseMenu,
		game
	};

	Game(int windowW, int windowH);
	~Game();

	GameState gState;

	void Start();
	void Update(float deltaTime);
	void DisplayMainMenu();

	Map* GetMap();
	Renderer* GetRenderer();
	Player* GetPlayer();
	Enemy* GetEnemy();
	SoundManager* GetSoundManager() { return this->soundManager; };

	//TODO: replace these with the vector method
	ParticleSystemWithTexture* getParticleSystem() { return this->smallAmmoParticle; };
	ParticleSystemWithTexture* getHeavyParticleSystem() { return this->heavyAmmoParticle; };

	// this vector holds all the player ammo that have been fired
	std::vector<Ammo*> vec_ammo;

	Explosion* explosion; //= Explosion(GameValues::explosionRadius, this);
	Explosion* heavyExplosion; //= Explosion(GameValues::heavyAmmoRadius, this);

	float getElapsedTimeAfterLastFrame() {return this->elapsed;};

	void setPlayerScore(int score) { this->playerScore = score; };
	void setEnemyScore(int score) { this->enemyScore = score; };

	int getPlayerScore() { return this->playerScore; };
	int getEnemyScore() { return this->enemyScore; };

	void RestartGame();
	void GoToMenu();
	void Pause();
	void Continue();

	void ResetScore();

	bool restartPending() { return this->restart; };

	bool getPlayerWon() { return this->playerWon; };
	bool getEnemyWon() { return this->enemyWon; };

	//used to init game once
	bool GameIsActive = false;

	bool GameIsPaused = false;

	void ScreenShake(float power);

	void ResetViewPos();
};