#pragma once

#include "SFML/Graphics.hpp"
#include <cstdlib>
#include <stdlib.h>
#include <time.h> 
#include "GameValues.h"

class Game;

class Map
{
private:
	unsigned int width = 250;
	unsigned int height = 250;

	unsigned int blockAmount = 25;
	unsigned int minBlockW = 50;

	sf::Uint8* mapPixels = nullptr;
	
	Game* m_game = nullptr;

	// This tells if the level data has changed
	bool bDirty = false;
public:
	Map(Game* game);
	~Map();

	int GetMinBlockW() { return this->minBlockW; };
	int GetWidth();
	int GetHeigth();
	void SetSize(int w, int h);
	void generateMap();
	void RedrawMap();
	//sf::Image* getMapData();

	sf::Sprite mapSprite;
	sf::Texture mapTexture;
	sf::Image* mapData = nullptr;

	void setDirty(bool b) { this->bDirty = b; };
	bool isDirty() { return this->bDirty; };
};

