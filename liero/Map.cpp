#include "Map.h"
#include "Game.h"


Map::Map(Game* game)
{
	this->m_game = game;
	this->blockAmount = GameValues::blockAmount;
	this->minBlockW = GameValues::minBlockW;
}

Map::~Map()
{
	//TODO: fix
	//delete mapPixels;
	//delete mapData;
}

int Map::GetWidth()
{
	return this->width;
}

int Map::GetHeigth()
{
	return this->height;
}

void Map::SetSize(int w, int h)
{
	this->width = w;
	this->height = h;
}

void Map::generateMap()
{
	if (mapPixels == nullptr)
	{
		mapPixels = new sf::Uint8[width*height*4];
	}

	if (mapData == nullptr)
	{
		mapData = new sf::Image();
		mapData->create(width, height, mapPixels);
	}

	for (int x = 0; x < width; x++)
		for (int y = 0; y < height; y++)
		{
			//sf::Color c = sf::Color(rand() % 256, rand() % 256, rand() % 256, 255);
			sf::Color c = GameValues::emptyColor;
			mapData->setPixel(x, y, c);
		}

	//TODO: Add magic numbers to GameValues
	for (int i = 0; i < blockAmount*0.75f; i++)
	{
		m_game->GetRenderer()->generateCircle(GameValues::breakAbleColor);
	}
	for (int i = 0; i < blockAmount*0.15f; i++)
	{
		m_game->GetRenderer()->generateCircle(GameValues::unbreakAbleColor);
	}

	mapTexture.loadFromImage(*mapData);
	sf::Sprite sprite;
	sprite.setTexture(mapTexture);
	this->bDirty = true;
	mapSprite = sprite;
}

void Map::RedrawMap()
{
	mapTexture.loadFromImage(*mapData);
	mapSprite.setTexture(mapTexture);
}

/*sf::Image* Map::getMapData()
{
	return mapData;
}*/
